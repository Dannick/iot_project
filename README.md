# Iot_Project

## BesteGroepOoit

## Description
For our IOT class on HAN applied university we designed and inplemented a LoRaWan Tester. We deployed the tester on the Helium network.

## Visuals
![image-3.png](./images/image-3.png)


## Installation
### <em><strong>1 - Arduino set-up</strong></em>
Make sure you have installed an Arduino IDE and add the Cubecell board to your board manager. the Json for the CubeCell board can be found at:

 https://github.com/HelTecAutomation/CubeCell-Arduino/releases/download/V1.4.0/package_CubeCell_index.json

 
When installed, deactivate the LORAWAN_RGB setting at Tools->LORAWAN_RGB->DEACTIVATE

### <em><strong>2 - Helium set-up</strong></em>
In the [Helium Console](https://console.helium.com) first add a new device.
![image-4.png](./images/image-4.png)

Give your device a name and paste the Dev-EUI, App-EUI and App-Key in State_Initialize (in Statemachine.cpp)


add a new Function to Helium for decoding of the payload send by the LorawanTester
![AddFunction.png](./images/image.png)



add the decoder from decoder/decoder.js into the helium custom decoder
![image-1.png](./images/image-1.png)



add a Intergration
![Intergration.png](./images/Intergration.png)



Set up the flow for the intergration
![Flow.png](./images/Flow.png)



## Installing the Software on the CubeCell
With the CubeCell connected to your pc and board added to the Arduino IDE open IOT_Tester/IOT_Tester.ino and upload the sketch to the board (with the right Dev-EUI, App-EUI and App-Key). Once the software is installed, when the device is turned on it automatically start sending to the Helium network.


