/**
 * @file Events.h
 * @author Jop Merz
 * @brief 
 * @version 0.1
 * @date 14-03-2022
 * 
 * @copyright Copyright (c) 2022
 */

#pragma once

enum EVENT{   NO_EVENT = 0,
              EVENT_SEQUENCE,
              EVENT_BUTTON_0,
              EVENT_BUTTON_1,
              EVENT_BUTTON_2,
              EVENT_BUTTON_3};
