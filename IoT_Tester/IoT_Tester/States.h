/**
 * @file States.h
 * @author Jop Merz
 * @brief 
 * @version 0.1
 * @date 14-03-2022
 * 
 * @copyright Copyright (c) 2022

 */


#pragma once

enum STATE{   NO_STATE = 0,
              STATE_INITIALIZE,
              STATE_DEVICE_SLEEP,
              STATE_LORAWAN_JOIN,
              STATE_LORAWAN_SEND,
              STATE_GNSS_START,
              STATE_GNSS_FIX,
              STATE_GNSS_GETDATA,
              STATE_GNSS_WAIT,
              STATE_GNSS_STOP};
