/**
 * @file Encoder.h
 * @author Jop Merz
 * @brief 
          Class which encodes (serialize) the folowing datatypes:
          8 bit, 16 bit, 24 bit 32 bit and 64 bit unsigned integers
          8 bit, 16 bit, 24 bit 32 bit and 64 bit signed integers (WIP)
          floats (4 byte) and doubles (8 byte)
 * @version 0.1
 * @date 23-02-2022
 * 
 * @copyright Copyright (c) 2022
*/

#pragma once

#include "Arduino.h"


class Encoder
{
public:
/**
 * @brief Construct a new Encoder object
 * 
 * @param buffer Pointer to the buffer of the encoder
 * @param size   Size of the buffer of the encoder in bytes
 */
    Encoder(uint8_t *buffer, uint32_t size);


public:
/**
 * @brief Sets the writing m_index value to 0
 * 
 */
    void ClearBuffer();


/**
 * @brief Get the Buffer pointer (uint_8t*) of the encoder
 * 
 * @return uint8_t* 
 */
    uint8_t *GetBuffer();


/**
 * @brief Get the Payload Size 
 * 
 * @return uint32_t 
 */
    uint32_t GetPayloadSize();


/**
 * @brief Writes an unsigned integer of 8 bits to the buffer
 * Increases m_index (payload Size) by 1 (8/8 = 1)
 * 
 * @param data 
 */
    void WriteUINT8(uint8_t data);


/**
 * @brief Writes an unsigned integer of 16 bits to the buffer
 * Increases m_index (payload Size) by 2 (16/8 = 2)
 * 
 * @param data 
 */
    void WriteUINT16(uint16_t data);


/**
 * @brief Writes an unsigned integer of 24 bits to the buffer
 * Increases m_index (payload Size) by 3 (24/8 = 3)
 * 
 * @param data 
 */
    void WriteUINT24(uint32_t data);


/**
 * @brief Writes an unsigned integer of 32 bits to the buffer
 * Increases m_index (payload Size) by 8 (32/8 = 4)
 * 
 * @param data 
 */
    void WriteUINT32(uint32_t data);


/**
 * @brief Writes an unsigned integer of 64 bits to the buffer
 *        Increases m_index (payload Size) by 8 (64/8 = 8)
 * 
 * @param data 
 */
    void WriteUINT64(uint64_t data);


/**
 * @brief Writes an signed integer of 8 bits to the buffer
 *        Increases m_index (payload Size) by 1 (8/8 = 1)
 * @param data 
 */
    void WriteINT8(int8_t data);


/**
 * @brief Writes an signed integer of 8 bits to the buffer
 *        Increases m_index (payload Size) by 2 (16/8 = 2)
 * @param data 
 */
    void WriteINT16(int16_t data);


/**
 * @brief Writes an signed integer of 8 bits to the buffer
 *        Increases m_index (payload Size) by 3 (24/8 = 3)
 * @param data 
 */
    void WriteINT24(int32_t data);


/**
 * @brief Writes an signed integer of 8 bits to the buffer
 *        Increases m_index (payload Size) by 4 (32/8 = 4)
 * @param data 
 */
    void WriteINT32(int32_t data);


/**
 * @brief Writes an signed integer of 8 bits to the buffer
 *        Increases m_index (payload Size) by 8 (64/8 = 8)
 * @param data 
 */
    void WriteINT64(int64_t data);


/**
 * @brief Writes a float of 4 bytes to the buffer
 * 
 * @param data 
 */
    void WriteFLOAT(float data);


/**
 * @brief Writes a double of 8 bytes to the buffer
 * 
 * @param data 
 */
    void WriteDOUBLE(double data);


private:
/**
 * @brief Pointer tot the external buffer
 * 
 */
    uint8_t *m_buffer;


/**
 * @brief The size of the external buffer
 * 
 */
    uint32_t m_size;


/**
 * @brief The writing index of the buffer where the next data set will be written 
 *        This also acts as the payload size
 * 
 */
    uint32_t m_index;
};
