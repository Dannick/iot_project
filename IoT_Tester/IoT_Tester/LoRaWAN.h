/**
 * @file LoRaWAN.h
 * @authors Heltec Automation, Jop Merz
 * @brief 
 * @version 0.1
 * @date 23-02-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#pragma once

#include "utilities.h"
#include "LoRaWan_102.h"
#include "HardwareSerial.h"
#include "Arduino.h"
#if defined(__asr650x__)
#include "board.h"
#include "gpio.h"
#include "hw.h"
#include "low_power.h"
#include "spi-board.h"
#include "rtc-board.h"
#include "asr_timer.h"
#include "board-config.h"
#include "hw_conf.h"
#include <uart_port.h>
#endif

#define REGION_EU868 

class LoRaWAN
{
public:
  // structors

  /**
   * @brief Construct a new Low-power wide area network (LoRaWAN) object
   * 
   */
  LoRaWAN();
  
  // User functions

  /**
   * @brief Initialize a new Low-power wide area network (LoRaWAN) interface
   * 
   */
  void Init();


/**
 * @brief Attempting to join a LoRaWAN network
 *        Returns if successfull or failed
 * 
 * @return true 
 * @return false 
 */
  bool Join();


/**
 * @brief Resets all the settings to default settings
 * 
 */
  void ResetSettings();


/**
 * @brief Sends the payload to the device
 *        Returns if successfull or failed
 * 
 * @param payload 
 * @param payloadSize 
 * @return true 
 * @return false 
 */
  bool SendPayload(uint8_t *payload, uint8_t payloadSize);


  

  // Setters
  /**
   * @brief Set the Dev Eui
   * 
   * @param eui 
   */
  void SetDevEui(uint8_t *eui);


/**
 * @brief Set the App Eui
 * 
 * @param eui 
 */
  void SetAppEui(uint8_t *eui);


/**
 * @brief Set the App Key 
 * 
 * @param key 
 */
  void SetAppKey(uint8_t *key);


/**
 * @brief Set the Channel Mask 
 * 
 * @param mask 
 */
  void SetChannelMask(uint16_t *mask);


/**
 * @brief Set the adaptive datarate
 * 
 * @param ADR 
 */
  void SetADR(bool ADR);



/**
 * @brief Set the confirmed/unconfirmed messages
 * 
 * @param confirmed 
 */
  void SetConfirmed(bool confirmed);


/**
 * @brief Set the Datarate 
 * 
 * @param datarate 
 */
  void SetDatarate(uint8_t datarate);



/**
 * @brief Set the Amount Of retries of messages
 * 
 * @param amount 
 */
  void SetAmountOfRetries(uint8_t amount);


/**
 * @brief Set the Port 
 * 
 * @param port 
 */
  void SetPort(uint8_t port);


/**
 * @brief Set the Region 
 * 
 * @param region 
 */
  void SetRegion(LoRaMacRegion_t region);


/**
 * @brief Set the class of the device (A,B or C)
 * 
 * @param deviceClass 
 */
  void SetClass(DeviceClass_t deviceClass);




  // Getters
/**
 * @brief Returns if device is successfully joined or not
 * 
 * @return true 
 * @return false 
 */
  bool IsJoined();




private:
  //Callback definitions
/**
 * @brief MLME-Indication event function
 * 
 * @param [IN] mlmeIndication - Pointer to the indication structure.
 */
  static void MlmeIndication( MlmeIndication_t *mlmeIndication );


/**
 * @brief MLME-Confirm event function
 * 
 * @param [IN] mlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
  static void MlmeConfirm( MlmeConfirm_t *mlmeConfirm );


/**
 * @brief MCPS-Indication event function
 * 
 * @param [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
  static void McpsIndication( McpsIndication_t *mcpsIndication );


/**
 * @brief MCPS-Confirm event function
 * 
 * @param [IN] mcpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
  static void McpsConfirm( McpsConfirm_t *mcpsConfirm );

  // static sync functions
  /**
   * @brief Activate global channel
   * 
   * @param mask 
   */
  static void ActivateGlobalChannelMask(uint16_t *mask);


/**
 * @brief Set the Global Joined 
 * 
 * @param joined 
 */
  static void SetGlobalJoined(bool joined);


/**
 * @brief Get the Global Joined 
 * 
 * @return true 
 * @return false 
 */
  static bool GetGlobalJoined();



  /**
   * @brief Generate the DevEUI from the hardware ID
   * 
   */
  void GenerateDevEUI();




private:
  uint8_t   m_devEui[8];
  uint8_t   m_appEui[8];
  uint8_t   m_appKey[16];
  uint16_t  m_channelsMask[6];
  
  #ifdef REGION_US915  
  const uint8_t m_defaultDatarate{DR_3};
  #else
  const uint8_t m_defaultDatarate{DR_5};
  #endif
  
  const uint8_t m_defaultRetries{4};
  
  uint8_t m_datarate;  
  uint8_t m_retries;
  uint8_t m_port;

  LoRaMacPrimitives_t m_macPrimitive;
  LoRaMacCallback_t   m_macCallback;
  uint32_t            m_macState;

  DeviceClass_t       m_class;
  LoRaMacRegion_t     m_region;

  bool m_confirmed;
  bool m_ADR;
  bool m_joined;
};


/**
 * @brief Turn on the red,green or blue led on the onboard leds
 * 
 */
extern "C" void turnOnRGB(uint32_t color,uint32_t time);


/**
 * @brief Turn off the red,green or blue led on the onboard leds
 * 
 */
extern "C" void turnOffRGB(void);


/**
 * @brief 
 * 
 */
extern "C" void DownLinkDataHandle(McpsIndication_t *mcpsIndication);


/**
 * @brief 
 * 
 */
extern "C" void LwanDevParamsUpdate();


/**
 * @brief 
 * 
 */
extern "C" void DevTimeUpdated();
