
#include "Encoder.h"

Encoder::Encoder(uint8_t *buffer, uint32_t size) :
m_buffer(buffer) ,
m_size(size)
{
    ClearBuffer();
}

void Encoder::ClearBuffer()
{
    m_index = 0;
}

uint8_t *Encoder::GetBuffer()
{
    return m_buffer;
}

uint32_t Encoder::GetPayloadSize()
{
  return m_index;
}

void Encoder::WriteUINT8(uint8_t data)
{
    m_buffer[m_index] = data;
    m_index ++;
}

void Encoder::WriteUINT16(uint16_t data)
{
    WriteUINT8((data >> 8)     & 0xFF);
    WriteUINT8((data)          & 0xFF);
}

void Encoder::WriteUINT24(uint32_t data)
{
    WriteUINT8((data >> 16)    & 0xFF);
    WriteUINT8((data >> 8)     & 0xFF);
    WriteUINT8((data)          & 0xFF);
}

void Encoder::WriteUINT32(uint32_t data)
{
    WriteUINT16((data >> 16)    & 0xFFFF);
    WriteUINT16((data)          & 0xFFFF);
}

void Encoder::WriteUINT64(uint64_t data)
{
    WriteUINT32((data >> 32)   & 0xFFFFFFFF);
    WriteUINT32((data)         & 0xFFFFFFFF);
}

void Encoder::WriteINT8(int8_t data)
{
  uint8_t unsignedData = data + 128;
  
  WriteUINT8((unsignedData)          & 0xFF);
}

void Encoder::WriteINT16(int16_t data)
{
  uint16_t unsignedData = data + 32768;
  
  WriteUINT8((unsignedData >> 8)     & 0xFF);
  WriteUINT8((unsignedData)          & 0xFF);
}

void Encoder::WriteINT24(int32_t data)
{
  uint32_t unsignedData = data + 8388608;
  
  WriteUINT8((unsignedData >> 16)    & 0xFF);
  WriteUINT8((unsignedData >> 8)     & 0xFF);
  WriteUINT8((unsignedData)          & 0xFF);
}

void Encoder::WriteINT32(int32_t data)
{
  uint32_t unsignedData = data + 2147483648;
  
  WriteUINT8((unsignedData >> 24)    & 0xFF);
  WriteUINT8((unsignedData >> 16)    & 0xFF);
  WriteUINT8((unsignedData >> 8)     & 0xFF);
  WriteUINT8((unsignedData)          & 0xFF);
}

void Encoder::WriteFLOAT(float data)
{
    uint32_t rawData = *(reinterpret_cast<uint32_t*>(&data));
    WriteUINT32(rawData);
}

void Encoder::WriteDOUBLE(double data)
{
    uint64_t rawData = *(reinterpret_cast<uint64_t*>(&data));
    WriteUINT64(rawData);
}
