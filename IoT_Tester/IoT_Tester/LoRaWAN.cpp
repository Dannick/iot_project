
#include "LoRaWan.h"

#include "loramac/region/RegionEU868.h"
#include "loramac/region/RegionEU433.h"
#include "loramac/region/RegionKR920.h"
#include "loramac/region/RegionAS923.h"

//loraWan default when adr disabled
int8_t defaultDrForNoAdr;
int8_t currentDrForNoAdr;

//LoRaMAC references
static bool     *s_pJoined = NULL;
static uint16_t *s_pChannelsMask  = NULL;
extern uint32_t LoRaMacState;

//A 'do nothing' function for timer callbacks
static void WakeUpDummy() {};

//Not used but required to link with CubeCellLib.a
bool keepNet=false;

//################################################################
// structors
//################################################################

LoRaWAN::LoRaWAN()
{
  ResetSettings();
}

//################################################################
// User functions
//################################################################

void LoRaWAN::Init()
{
  if(m_region == LORAMAC_REGION_AS923_AS1 || m_region == LORAMAC_REGION_AS923_AS2)
  {
    m_region = LORAMAC_REGION_AS923;
  }    

  m_macPrimitive.MacMcpsConfirm     = McpsConfirm;
  m_macPrimitive.MacMcpsIndication  = McpsIndication;
  m_macPrimitive.MacMlmeConfirm     = MlmeConfirm;
  m_macPrimitive.MacMlmeIndication  = MlmeIndication;
  
  m_macCallback.GetBatteryLevel     = NULL;
  m_macCallback.GetTemperatureLevel = NULL;
  
  LoRaMacInitialization( &m_macPrimitive, &m_macCallback, m_region);

  MibRequestConfirm_t mibReq;
  mibReq.Type = MIB_ADR;
  mibReq.Param.AdrEnable = m_ADR;
  LoRaMacMibSetRequestConfirm( &mibReq );

  mibReq.Type = MIB_PUBLIC_NETWORK;
  mibReq.Param.EnablePublicNetwork = LORAWAN_PUBLIC_NETWORK;
  LoRaMacMibSetRequestConfirm( &mibReq );

  ActivateGlobalChannelMask(m_channelsMask);
  LwanDevParamsUpdate();

  mibReq.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm( &mibReq );
  
  if(m_class != mibReq.Param.Class)
  {
    mibReq.Param.Class = m_class;
    LoRaMacMibSetRequestConfirm( &mibReq );
  }
}

bool LoRaWAN::Join()
{
  Init();
  
  MlmeReq_t mlmeReq;    
  mlmeReq.Type = MLME_JOIN;

  mlmeReq.Req.Join.DevEui   = m_devEui;
  mlmeReq.Req.Join.AppEui   = m_appEui;
  mlmeReq.Req.Join.AppKey   = m_appKey;
  mlmeReq.Req.Join.NbTrials = 1;

  if ( LoRaMacMlmeRequest( &mlmeReq ) != LORAMAC_STATUS_OK ) 
  {
    return false;
  }

  //Wait until radio has finished trying to join
    TimerEvent_t pollStateTimer;
    TimerInit( &pollStateTimer, WakeUpDummy );
    TimerSetValue( &pollStateTimer, 100 );
    
  while (LoRaMacState!=LORAMAC_IDLE) 
  {
    TimerStart( &pollStateTimer );
    lowPowerHandler( );
    TimerStop( &pollStateTimer );
    Radio.IrqProcess( );
  }

  //Result will have been set via a callback
  m_joined = GetGlobalJoined();
  
  return m_joined;
}

void LoRaWAN::ResetSettings()
{
  memcpy(m_devEui, 0x00, sizeof(m_devEui));
  memcpy(m_appEui, 0x00, sizeof(m_appEui));
  memcpy(m_appKey, 0x00, sizeof(m_appKey));

  memcpy(m_channelsMask, 0x0000, sizeof(m_channelsMask));
  m_channelsMask[0] = 0x00FF;

  m_ADR       = true;
  m_confirmed = false;
  m_datarate  = m_defaultDatarate;
  m_retries   = m_defaultRetries;
  m_region    = LORAMAC_REGION_EU868;
  m_port      = 1;
  m_class     = CLASS_A;
}


bool LoRaWAN::SendPayload(uint8_t *payload, uint8_t payloadSize)
{
  MibRequestConfirm_t mibReq;
  mibReq.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm( &mibReq );

  if (m_class != mibReq.Param.Class) 
  {
    mibReq.Param.Class = m_class;
    LoRaMacMibSetRequestConfirm( &mibReq );
  } 

  ActivateGlobalChannelMask(m_channelsMask);
  LwanDevParamsUpdate();
  
  McpsReq_t mcpsReq;
  LoRaMacTxInfo_t txInfo;
  
  if ( LoRaMacQueryTxPossible( payloadSize, &txInfo ) != LORAMAC_STATUS_OK ) 
  {
    // Send empty frame in order to flush MAC commands
    mcpsReq.Type = MCPS_UNCONFIRMED;
    mcpsReq.Req.Unconfirmed.fBuffer     = NULL;
    mcpsReq.Req.Unconfirmed.fBufferSize = 0;
    mcpsReq.Req.Unconfirmed.Datarate    = m_datarate;
  } 
  else 
  {
    if (m_confirmed)  
    {
      mcpsReq.Type = MCPS_CONFIRMED;
      mcpsReq.Req.Confirmed.fPort       = m_port;
      mcpsReq.Req.Confirmed.fBuffer     = payload;
      mcpsReq.Req.Confirmed.fBufferSize = payloadSize;
      mcpsReq.Req.Confirmed.NbTrials    = m_retries;
      mcpsReq.Req.Confirmed.Datarate    = m_datarate;
    } 
    else 
    {
      mcpsReq.Type = MCPS_UNCONFIRMED;
      mcpsReq.Req.Unconfirmed.fPort       = m_port;
      mcpsReq.Req.Unconfirmed.fBuffer     = payload;
      mcpsReq.Req.Unconfirmed.fBufferSize = payloadSize;
      mcpsReq.Req.Unconfirmed.Datarate    = m_datarate;
    }
  }

  delay(5);
  
  if( LoRaMacMcpsRequest( &mcpsReq ) != LORAMAC_STATUS_OK )
  {
    //Bad LoRaMacMcpsRequest status, couldn't send
    return false;
  }

  TimerEvent_t pollStateTimer;
  TimerInit( &pollStateTimer, WakeUpDummy );
  TimerSetValue( &pollStateTimer, 100 );
    
  while (LoRaMacState!=LORAMAC_IDLE) 
  {
    TimerStart( &pollStateTimer );
    lowPowerHandler( );
    TimerStop( &pollStateTimer );
    Radio.IrqProcess( );
  }

  return true;
}

//################################################################
// Setters
//################################################################

void LoRaWAN::SetDevEui(uint8_t *eui)
{
  for(uint8_t i = 0; i < 8; i++)
  {
    m_devEui[i] = eui[i];
  }
}

void LoRaWAN::SetAppEui(uint8_t *eui)
{
  for(uint8_t i = 0; i < 8; i++)
  {
    m_appEui[i] = eui[i];
  }
}

void LoRaWAN::SetAppKey(uint8_t *key)
{
  for(uint8_t i = 0; i < 16; i++)
  {
    m_appKey[i] = key[i];
  }
}

void LoRaWAN::SetChannelMask(uint16_t *mask)
{
  for(uint8_t i = 0; i < 6; i++)
  {
    m_channelsMask[i] = mask[i];
  }
}

void LoRaWAN::SetADR(bool ADR)
{
  m_ADR = ADR;
}

void LoRaWAN::SetConfirmed(bool confirmed)
{
  m_confirmed = confirmed;
}

void LoRaWAN::SetDatarate(uint8_t datarate)
{
  m_ADR       = false;
  m_datarate  = datarate;
}

void LoRaWAN::SetAmountOfRetries(uint8_t amount)
{
  m_retries = amount;
}

void LoRaWAN::SetPort(uint8_t port)
{
  m_port = port;
}

void LoRaWAN::SetRegion(LoRaMacRegion_t region)
{
  m_region = region;
}

void LoRaWAN::SetClass(DeviceClass_t deviceClass)
{
  m_class = deviceClass;
}


void LoRaWAN::GenerateDevEUI()
{
  uint64_t chipID=getID();
   
  for(int i=7;i>=0;i--)
  {
    m_devEui[i] = (chipID>>(8*(7-i)))&0xFF;
  }
}

void LoRaWAN::McpsConfirm( McpsConfirm_t *mcpsConfirm )
{
  if( mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
  {
    switch( mcpsConfirm->McpsRequest )
    {
      case MCPS_UNCONFIRMED:
      {
        // Check Datarate
        // Check TxPower
        break;
      }
      
      case MCPS_CONFIRMED:
      {
        // Check Datarate
        // Check TxPower
        // Check AckReceived
        // Check NbTrials
        if (mcpsConfirm->AckReceived) 
        {
          //printf("ACK was received");
        } else 
        {
          //printf("ACK was NOT received");
        }
        break;
      }
      
      case MCPS_PROPRIETARY:      
        break;
      
      default:
        break;
    }
  }
}

void LoRaWAN::McpsIndication( McpsIndication_t *mcpsIndication )
{
  if( mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK )
  {
    return;
  }

  switch( mcpsIndication->McpsIndication )
  {
    case MCPS_UNCONFIRMED:    
      break;
    
    case MCPS_CONFIRMED:    
      break;
    
    case MCPS_PROPRIETARY:    
      break;
    
    case MCPS_MULTICAST:    
      break;
    
    default:
      break;
  }

  // Check Multicast
  // Check Port
  // Check Datarate
  // Check FramePending
  if( mcpsIndication->FramePending == true )
  {
    // The server signals that it has pending data to be sent.
    // We schedule an uplink as soon as possible to flush the server.
  }
  // Check Buffer
  // Check BufferSize
  // Check Rssi
  // Check Snr
  // Check RxSlot
  if( mcpsIndication->RxData == true )
  {
    DownLinkDataHandle(mcpsIndication);
  }
}

void LoRaWAN::MlmeConfirm( MlmeConfirm_t *mlmeConfirm )
{
  switch( mlmeConfirm->MlmeRequest )
  {
    case MLME_JOIN:
    {
      if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
      {
        SetGlobalJoined(true);
      } 
      else 
      {
        SetGlobalJoined(false);
      }      
      break;
    }
    
    case MLME_LINK_CHECK:
    {
      if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
      {
        // Check DemodMargin
        // Check NbGateways
      }
      break;
    }
    
    case MLME_DEVICE_TIME:
    {
      if( mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK )
      {
        DevTimeUpdated();
      }
      break;
    }
    
    default:
      break;
  }
}

void LoRaWAN::MlmeIndication( MlmeIndication_t *mlmeIndication )
{
  switch( mlmeIndication->MlmeIndication )
  {
    case MLME_SCHEDULE_UPLINK:
    //The MAC signals that we shall provide an uplink as soon as possible
      break;
    
    default:
      break;
  }
}

void LoRaWAN::ActivateGlobalChannelMask(uint16_t *mask)
{
  s_pChannelsMask = mask;
}

void LoRaWAN::SetGlobalJoined(bool joined)
{
  if (s_pJoined != NULL)
  {
    *s_pJoined = joined;
  }  
}

bool LoRaWAN::GetGlobalJoined()
{
  return s_pJoined;
}
  
void __attribute__((weak)) DownLinkDataHandle(McpsIndication_t *mcpsIndication)
{

}


void __attribute__((weak)) DevTimeUpdated()
{

}

void LwanDevParamsUpdate()
{
#if defined( REGION_EU868 )
  LoRaMacChannelAdd( 3, ( ChannelParams_t )EU868_LC4 );
  LoRaMacChannelAdd( 4, ( ChannelParams_t )EU868_LC5 );
  LoRaMacChannelAdd( 5, ( ChannelParams_t )EU868_LC6 );
  LoRaMacChannelAdd( 6, ( ChannelParams_t )EU868_LC7 );
  LoRaMacChannelAdd( 7, ( ChannelParams_t )EU868_LC8 );
#elif defined( REGION_EU433 )
  LoRaMacChannelAdd( 3, ( ChannelParams_t )EU433_LC4 );
  LoRaMacChannelAdd( 4, ( ChannelParams_t )EU433_LC5 );
  LoRaMacChannelAdd( 5, ( ChannelParams_t )EU433_LC6 );
  LoRaMacChannelAdd( 6, ( ChannelParams_t )EU433_LC7 );
  LoRaMacChannelAdd( 7, ( ChannelParams_t )EU433_LC8 );
#elif defined( REGION_KR920 )
  LoRaMacChannelAdd( 3, ( ChannelParams_t )KR920_LC4 );
  LoRaMacChannelAdd( 4, ( ChannelParams_t )KR920_LC5 );
  LoRaMacChannelAdd( 5, ( ChannelParams_t )KR920_LC6 );
  LoRaMacChannelAdd( 6, ( ChannelParams_t )KR920_LC7 );
  LoRaMacChannelAdd( 7, ( ChannelParams_t )KR920_LC8 );
#elif defined( REGION_AS923 ) || defined( REGION_AS923_AS1 ) || defined( REGION_AS923_AS2 )
  LoRaMacChannelAdd( 2, ( ChannelParams_t )AS923_LC3 );
  LoRaMacChannelAdd( 3, ( ChannelParams_t )AS923_LC4 );
  LoRaMacChannelAdd( 4, ( ChannelParams_t )AS923_LC5 );
  LoRaMacChannelAdd( 5, ( ChannelParams_t )AS923_LC6 );
  LoRaMacChannelAdd( 6, ( ChannelParams_t )AS923_LC7 );
  LoRaMacChannelAdd( 7, ( ChannelParams_t )AS923_LC8 );
#endif

  MibRequestConfirm_t mibReq;

  mibReq.Type = MIB_CHANNELS_DEFAULT_MASK;
  mibReq.Param.ChannelsMask = s_pChannelsMask;
  LoRaMacMibSetRequestConfirm(&mibReq);

  mibReq.Type = MIB_CHANNELS_MASK;
  mibReq.Param.ChannelsMask = s_pChannelsMask;
  LoRaMacMibSetRequestConfirm(&mibReq);
}
