

#include "Arduino.h"
#include "Encoder.h"
#include "Statemachine.h"

const float Statemachine::s_earthHorizontalDiameter = 6356000.0f;
const float Statemachine::s_earthVerticalDiameter   = 6378000.0f;

 // addr , freq , SDA, SCL, resolution , rst
SSD1306Wire display(60, 500000, SDA, SCL, GEOMETRY_128_64, GPIO10);


Statemachine::Statemachine()
{
    Initialize();
}

void Statemachine::VextON()
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, LOW);
}

void Statemachine::VextOFF()
{
  pinMode(Vext,OUTPUT);
  digitalWrite(Vext, HIGH);
}

//######################################################
// Statemachine logic
//######################################################

void Statemachine::Initialize()
{
    m_currentState = STATE_INITIALIZE;
    m_currentEvent = NO_EVENT;
    
    m_mode = MODE_AREA;

    m_signal = SIGNAL_NONE;
    m_radiusThreshold = 60;
    m_intervalTime    = 10000;
    m_GNSSWaitTime    = 1000;
    m_messagesSend    = 0;
    m_distance        = 0;

    m_previousCoord.x = 0.0f;
    m_previousCoord.y = 0.0f;
}

void Statemachine::Update()
{
    switch (m_currentState)
    {
    case NO_STATE:
      SetNextState(STATE_INITIALIZE);
      break;

    case STATE_INITIALIZE:
      {
        // OLED display on
        VextON();// oled power on;
        delay(10); 
        display.init();
        delay(10);
        
        {
          char statusString[] = "Starting...";
          strncpy(m_statusString, statusString, sizeof(statusString));      
          UpdateDisplay();
        }
        
        m_COM.ResetSettings();
  
        uint8_t devEui[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        uint8_t appEui[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        uint8_t appKey[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        uint16_t mask[]  = { 0x00FF,0x0000,0x0000,0x0000,0x0000,0x0000 };

        m_COM.SetDevEui(devEui);
        m_COM.SetAppEui(appEui);
        m_COM.SetAppKey(appKey);
        m_COM.SetChannelMask(mask);
        
        m_COM.SetADR(false);
        m_COM.SetConfirmed(false);
        m_COM.SetAmountOfRetries(4);
        m_COM.SetPort(2);
        m_COM.SetRegion(LORAMAC_REGION_EU868);
        m_COM.SetClass(CLASS_A);
        m_COM.SetDatarate(DR_4);

        m_COM.Init();
      }

      SetNextState(STATE_LORAWAN_JOIN);
      break;

    case STATE_LORAWAN_SEND:
      {
        char statusString[] = "Sending...";
        strncpy(m_statusString, statusString, sizeof(statusString));      
        UpdateDisplay();
        
        uint8_t   protocolID    = 1;
        float     GPSLatitude   = static_cast<float>    (m_GPS.location.lat());
        float     GPSLongitude  = static_cast<float>    (m_GPS.location.lng());
        int16_t   GPSAltitude   = static_cast<int16_t>  (m_GPS.altitude.meters() * 100);
        uint8_t   GPShdop       = static_cast<uint8_t>  (m_GPS.hdop.hdop() * 10);
        uint8_t   GPSsatellites = static_cast<uint8_t>  (m_GPS.satellites.value());

        Encoder encoder(m_payload, PACKAGE_BUFFER_SIZE);
        encoder.ClearBuffer();

        // payload, total of 13 bytes
        //-----------------------------------
        encoder.WriteUINT8(protocolID);
        encoder.WriteFLOAT(GPSLatitude);
        encoder.WriteFLOAT(GPSLongitude);
        encoder.WriteINT16(GPSAltitude);
        encoder.WriteUINT8(GPShdop);
        encoder.WriteUINT8(GPSsatellites);
        //-----------------------------------
        
        m_payloadSize = encoder.GetPayloadSize();
      }

      m_COM.SendPayload(m_payload, m_payloadSize);
      m_messagesSend ++;

      SetNextState(STATE_GNSS_WAIT);
      break;

    case STATE_LORAWAN_JOIN: 
      {
        char statusString[] = "Joining...";
        strncpy(m_statusString, statusString, sizeof(statusString));
        UpdateDisplay();
      }
      
      m_COM.Join();
                  
      SetNextState(STATE_GNSS_START);
      break;

    case STATE_GNSS_START: 
      {
        char statusString[] = "Starting GPS...";
        strncpy(m_statusString, statusString, sizeof(statusString));
        UpdateDisplay();        
      }
      
      m_GPS.begin();
      SetNextState(STATE_GNSS_FIX);
      break;

    case STATE_GNSS_FIX:
      {              
        char statusString[] = "Searching GPS...";
        strncpy(m_statusString, statusString, sizeof(statusString));
        UpdateDisplay();
      
        uint32_t start = millis();
        while( (millis()-start) < 60000 )
        {
          while (m_GPS.available() > 0)
          {
            m_GPS.encode(m_GPS.read());
          }

          // time since last update, under 1500 ms means fix
          if(m_GPS.location.age() < 1500)
          {
            break;
          }
        }
      }

      if(m_GPS.location.age() < 1500)
      {
        SetNextState(STATE_GNSS_GETDATA); // GPS fix
      }
      else
      {
        SetNextState(STATE_GNSS_FIX); // no GPS fix
      }
      
      break;

    case STATE_GNSS_GETDATA:
      {
        while (m_GPS.available() > 0)
        {
          m_GPS.encode(m_GPS.read());
        }

        switch(m_mode)
        {
          case MODE_TRIGGER:
            SetNextState(STATE_GNSS_STOP);
            break;

          case MODE_INTERVAL:
            SetNextState(STATE_LORAWAN_SEND);
            break;

          case MODE_AREA:
            {
              // new lat and lng relative from old lat and lng in degrees
              float deltaX = (m_GPS.location.lng() - m_previousCoord.x);
              float deltaY = (m_GPS.location.lat() - m_previousCoord.y);

              // from degrees to meters
              deltaX = deltaX / 180.0f * s_earthHorizontalDiameter;
              deltaY = deltaY / 90.0f  * s_earthVerticalDiameter;

              float distance = sqrt(deltaX*deltaX + deltaY*deltaY);
    
              m_distance = static_cast<uint16_t> (distance);
    
              if (distance >= m_radiusThreshold)
              {
                m_previousCoord.x = m_GPS.location.lng();
                m_previousCoord.y = m_GPS.location.lat();
                
                SetNextState(STATE_LORAWAN_SEND);
              }
              else
              {
                SetNextState(STATE_GNSS_WAIT);
              }
            }

            break;

            default:
              SetNextState(STATE_INITIALIZE);
              break;
        }


        if (m_GPS.hdop.hdop() <= 2.0)
        {
          m_signal = SIGNAL_EXCELLENT;
        }          
        else if (m_GPS.hdop.hdop() <= 5.0)
        {
          m_signal = SIGNAL_GOOD;
        }
        else if (m_GPS.hdop.hdop() <= 10.0)
        {
          m_signal = SIGNAL_MODERATE;
        }
        else if (m_GPS.hdop.hdop() <= 20.0)
        {
          m_signal = SIGNAL_POOR;
        }
        else
        {
          m_signal = SIGNAL_TERRIBLE;
        }

        if (m_GPS.satellites.value() <= 2)
        {
          m_signal = SIGNAL_NONE;
        }

        // cast to int to eliminate floating point rounding errors in check
        int8_t lat = static_cast<int8_t> (m_GPS.location.lat());
        int8_t lng = static_cast<int8_t> (m_GPS.location.lng());
        if (!m_GPS.location.isValid() || lat == 0 || lng == 0)
        {
          SetNextState(STATE_GNSS_FIX);   // not valid, try again
          m_signal = SIGNAL_NONE;
        }
      }
      break;

    case STATE_GNSS_WAIT:
      {
        {
          char statusString[] = "Waiting...";
          strncpy(m_statusString, statusString, sizeof(statusString));    
          UpdateDisplay();
        }

        uint32_t waitTime = 0;

        if (m_mode == MODE_INTERVAL)
        {
          waitTime = m_intervalTime;
        }
        
        if (m_mode == MODE_AREA)
        {
          waitTime = m_GNSSWaitTime;
        }
      
        uint32_t start = millis();
        while( (millis()-start) < waitTime )
        {
          while (m_GPS.available() > 0)
          {
            m_GPS.encode(m_GPS.read());
          }
        }
      }

      if(m_GPS.location.age() < 1500)
      {
        SetNextState(STATE_GNSS_GETDATA); // GPS fix
      }
      else
      {
        SetNextState(STATE_GNSS_FIX); // no GPS fix
      }
      
      break;

    case STATE_GNSS_STOP:
      m_GPS.end();
      SetNextState(STATE_GNSS_WAIT);
      break;

    default:
      SetNextState(STATE_INITIALIZE);
      break;
    }

    m_currentState = m_nextState;
}

void Statemachine::Reset()
{
    Initialize();
}



void Statemachine::UpdateDisplay()
{
  char str[30];
  display.clear();
  display.setFont(ArialMT_Plain_10);
  int index = sprintf(str,"Status: %s", m_statusString);
  str[index] = 0;
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 0, str);

  char signalString[6][16] = {"EXCELLENT", "GOOD", "MODERATE", "POOR", "TERRIBLE", "NO SIGNAL"};
       
  index = sprintf(str,"Signal: %s", signalString[m_signal]);
  str[index] = 0;
  display.drawString(0, 16, str);

  char modeString[3][10] = {"TRIGGER", "INTERVAL", "AREA"};
      
  index = sprintf(str,"Mode : %s", modeString[m_mode]);
  str[index] = 0;
  display.drawString(0, 32, str);
      
  index = sprintf(str,"Send : %i", m_messagesSend);
  str[index] = 0;
  display.drawString(0, 48, str);
  display.display();

  index = sprintf(str,"Dis: %i m", m_distance);
  str[index] = 0;
  display.drawString(64, 48, str);
  display.display();
}

//######################################################
// Getters & setters
//######################################################

void Statemachine::SetNextState(STATE state)
{
  m_nextState = state;
}

void Statemachine::SetEvent(EVENT event)
{
  m_currentEvent = event;
}

EVENT Statemachine::GetCurrentEvent()
{
  return m_currentEvent;
}

STATE Statemachine::GetCurrentState()
{
  return m_currentState;
}
