/**
 * @file Statemachine.h
 * @author Jop Merz
 * @brief 
 * @version 0.1
 * @date 15-03-2022
 * 
 * @copyright Copyright (c) 2022
 * 
 */


#pragma once

#include "States.h"
#include "Events.h"

#include "GPS_Air530Z.h"
#include "HT_SSD1306Wire.h"
#include "LoRaWAN.h"

#define PACKAGE_BUFFER_SIZE 255

/**
 * @brief This ENUM indicates if the device sends with an button, set interval or when the threshold exceeds a certain value
 * 
 */
enum MODE{MODE_TRIGGER, MODE_INTERVAL, MODE_AREA};

/**
 * @brief This ENUM indicates the different values depending on the quality of the GPS signal
 * 
 */
enum SIGNAL{SIGNAL_EXCELLENT, SIGNAL_GOOD, SIGNAL_MODERATE, SIGNAL_POOR, SIGNAL_TERRIBLE, SIGNAL_NONE};

typedef struct Point
{
  float x, y;
};

/** 
\dot

digraph AREA_STATE_DIAGRAM 
  {    
    node [shape=point,label=""]ENTRY;  
    node [shape=rect, style=rounded, fontname=Helvetica, fontsize=10];
       
    STATE_INITIALIZE[label="INITIALIZE"];  
    STATE_LORAWAN_JOIN[label="LORAWAN_JOIN"];
    STATE_LORAWAN_SEND[label="LORAWAN_SEND"];  
    STATE_GNSS_START[label="GNSS_START"];
    STATE_GNSS_FIX[label="GNSS_FIX"];  
    STATE_GNSS_GETDATA[label="GNSS_GETDATA"];
    STATE_GNSS_WAIT[label="GNSS_WAIT"];
     
    ENTRY->STATE_INITIALIZE [label="Initialization"];
    STATE_INITIALIZE->STATE_LORAWAN_JOIN; 
    STATE_LORAWAN_JOIN->STATE_GNSS_START [label="JOINED"];
    STATE_GNSS_START->STATE_GNSS_FIX [label="GNSS_STARTED"];
    STATE_GNSS_FIX->STATE_GNSS_GETDATA [label="GNSS_FIXED"];
    STATE_GNSS_GETDATA->STATE_LORAWAN_SEND [label="GNSS_DATA_PRESENT"];
    STATE_LORAWAN_SEND->STATE_GNSS_WAIT;
    STATE_GNSS_WAIT->STATE_GNSS_GETDATA [label="wait for 1 second"];
  }

\enddot 
*/

class Statemachine
{
public:
/**
 * @brief Construct a new Statemachine object
 * 
 */
    Statemachine();


/**
 * @brief Set the Event 
 * 
 * @param event 
 */
    void SetEvent(EVENT event);


/**
 * @brief Initialze the device with selected values
 * 
 */
    void Initialize();


/**
 * @brief Depending on the current state, the statemachine will update specific logic
 * 
 */
    void Update();


/**
 * @brief Get the Current Event 
 * 
 * @return EVENT 
 */
    EVENT GetCurrentEvent();


/**
 * @brief Get the Current State 
 * 
 * @return STATE 
 */
    STATE GetCurrentState();


private:    
/**
 * @brief Set default settings
 * 
 */
    void Reset(); 


/**
 * @brief Set the Next State 
 * 
 * @param state 
 */
    void SetNextState(STATE state);


/**
 * @brief Updates display depending on signal quality, current operating mode, total send packages, distance between last message send and current location and current device status
 * 
 */
    void UpdateDisplay();

    
/**
 * @brief Turn OLED display on
 * 
 */
    void VextON();


/**
 * @brief Turn OLED display off
 * 
 */
    void VextOFF();


private:
/**
 * @brief Indicates which event is the current event of the statemachine
 * 
 */
    EVENT m_currentEvent;


/**
 * @brief Indicates which state is the current state of the state machine
 * 
 */
    STATE m_currentState;


/**
 * @brief Indicates which state is the next state of the state machine
 * 
 */
    STATE m_nextState;


/**
 * @brief Indicates which mode is the current mode of the device
 * 
 */
    MODE  m_mode;


/**
 * @brief Indicates what the current signal quality is
 * 
 */
    SIGNAL m_signal;


/**
 * @brief Indicates what the current interval time is
 * 
 */
    uint16_t m_intervalTime;


/**
 * @brief The refresh rate of the GNSS mode. This is usally 1 second because GNSS satellites have a refresh rate of 1 hz
 * 
 */
    uint16_t m_GNSSWaitTime;


/**
 * @brief Indicates the difference between the last message send and the current location
 * 
 */
    uint16_t m_radiusThreshold;


/**
 * @brief Indicates the location of the time when the last message whas send
 * 
 */
    Point m_previousCoord;



/**
 * @brief Object which provides the communication over LoRaWAN and the device
 * 
 */
    LoRaWAN       m_COM;


/**
 * @brief Object which provides the communication over the GPS and the device
 * 
 */
    Air530ZClass  m_GPS;


/**
 * @brief The buffer of the LoRaWAN 
 * 
 */
    uint8_t m_payload[PACKAGE_BUFFER_SIZE];


/**
 * @brief The size of buffer of the LoRaWAN 
 * 
 */
    uint8_t m_payloadSize;


/**
 * @brief The amount of send messages
 * 
 */
    uint16_t m_messagesSend;


/**
 * @brief The distance between the last location a message was send and the current location
 * 
 */
    uint16_t m_distance;


/**
 * @brief Current status of the device in string form
 * 
 */
    char m_statusString[16];


/**
 * @brief Diameter of the earth at the equator
 * 
 */
    static const float s_earthHorizontalDiameter;


/**
 * @brief Distance from pole to pole of the earth in meters
 * 
 */
    static const float s_earthVerticalDiameter;
};
