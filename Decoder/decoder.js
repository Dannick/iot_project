//########################################
// Standard decoder methods
//########################################

class DecoderClass
{
  constructor(payload)
  {
    this.index   = 0;
    this.payload = payload;
  }
  
  // range: 0 to 255
  DecodeUINT8()
  {
    var data = this.payload[this.index ++];
    
    return data;
  }
  
  // range: 0 to 65 536
  DecodeUINT16()
  {
    var data = 0;
    
    data |= (this.payload[this.index ++] << 8);
    data |= (this.payload[this.index ++]);    
    
    return data;
  }
  
  // range: 0 to 16 777 215
  DecodeUINT24()
  {
    var data = 0;
    
    data |= (this.payload[this.index ++] << 16);
    data |= (this.payload[this.index ++] << 8);
    data |= (this.payload[this.index ++]);    
    
    return data;
  }
  
  // range: 0 to 4 294 967 295
  DecodeUINT32()
  {
    var data = 0;
    
    data |= (this.payload[this.index ++] << 24);
    data |= (this.payload[this.index ++] << 16);
    data |= (this.payload[this.index ++] << 8);
    data |= (this.payload[this.index ++]);    
    
    return data;
  }
  
  // range: -128 to 127
  DecodeINT8()
  {
    var data = this.DecodeUINT8();    
    return data - 128;
  }
  
  // range: -32 768 to +32 767
  DecodeINT16()
  {
    var data = this.DecodeUINT16();    
    return data - 32768;
  }
  
  // range: −8 388 608 to 8 388 607
  DecodeINT24()
  {
    var data = this.DecodeUINT24();
    return data - 8388608;
  }
  
  // range: -2 147 483 648 to 2 147 483 647
  DecodeINT32()
  {
    var data = this.DecodeUINT32();
    return data - 2147483648;
  }
  
  DecodeFLOAT()
  {
    var buffer = new ArrayBuffer(4);
    var bytes  = new Uint8Array(buffer);

    bytes[0] = (this.payload[this.index ++]);
    bytes[1] = (this.payload[this.index ++]);
    bytes[2] = (this.payload[this.index ++]);
    bytes[3] = (this.payload[this.index ++]);
    
    var view   = new DataView(buffer);   
    
    return view.getFloat32(0, false);
  }
}

//########################################
// Decoder
//########################################

function Decoder(bytes, port) 
{  
  const decoder = new DecoderClass(bytes);
    
  const protocol   = decoder.DecodeUINT8();
  const latitude   = decoder.DecodeFLOAT();
  const longtitude = decoder.DecodeFLOAT();
  const altitude   = decoder.DecodeUINT16() / 100;   
  const accuracy   = 6;
  const hdop       = decoder.DecodeUINT8() / 10;  
  const satellites = decoder.DecodeUINT8();
  const vBatt      = decoder.DecodeUINT16() / 1000;

  var decodedPayload = 
  {
      latitude:   latitude,
      longitude:  longtitude,
      altitude:   altitude,
      accuracy:   accuracy,
      HDOP:       hdop,
      satellites: satellites,
      vBatt:      vBatt
  };
 
  return decodedPayload;
}